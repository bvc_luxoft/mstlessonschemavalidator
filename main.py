from jsonschema import validate, Draft7Validator,Draft3Validator

import json
import requests

def getLessonsFromFile():
    json_file = {}
    with open('GetLessonsPg=0.json', encoding='utf8') as f:
        json_file = json.load(f)
    return json_file

def getLessonsFromURL(url):
    headers = {'Authorization': 'Bearer Authorization'}
    response = requests.request("GET", url, headers=headers, data={})
    #print(response.text)
    return json.loads(response.text)

def mst_validate(validated_schema, json_to_validate):
    error_count = 0
    for lesson in json_to_validate:
        data = lesson['data']
        errors = sorted(validated_schema.iter_errors(data), key=lambda e: e.path)
        #validate(instance=data, schema=schema)

        if len(errors) > 0:
            id = lesson['id']
            title = data['customTitle']
            print(f'LessonID: {id} Title: "{title}"')
            for error in errors:
                error_count += 1
                print(f'\t{error.absolute_path}')
                print(f'\t{error.message}')
                #for suberror in sorted(error.context, key=lambda e: e.schema_path):
    if error_count < 1:
        print(f'Good Job!  No Errors found in all {len(json_to_validate)} lessons! :)')

def getValidatedSchema():
    v = None
    with open('LessonSchema.json') as f:
      #schema = {"type": "object", "properties": {"price": {"type": "number"}, "name": {"type": "string"}}} #fake for code-testing
      schema = json.load(f)
      v = Draft7Validator(schema)
    return v


def validateAgainstDisk(validated_schema):
    json_to_validate = getLessonsFromFile()
    mst_validate(validated_schema=validated_schema,
                 json_to_validate=json_to_validate)


def validateAgainstAPIs(validated_schema):
    urls = ["https://ms30-functions.azurewebsites.net/api/v1/lessons/?offset=0&limit=100&minormodel=737-8",
            "https://ms30-functions.azurewebsites.net/api/v1/lessons/?offset=0&limit=100&minormodel=787-9",
            "https://ms30-functions.azurewebsites.net/api/v1/lessons/?offset=0&limit=100&minormodel=777-8"]

    for api_url in urls:
        print(f'\nValidating JSON from: "{api_url}"')
        print("============================================")

        json_to_validate = getLessonsFromURL(api_url)
        # print(getLatestJSON())
        mst_validate(validated_schema=validated_schema,
                     json_to_validate=json_to_validate)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    validated_schema = getValidatedSchema()
    validateAgainstAPIs(validated_schema)
    # validateAgainstDisk(validated_schema)
